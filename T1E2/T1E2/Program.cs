﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1E2
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Cody";
            Console.WriteLine($"Hello {name}!");
        }
    }
}
