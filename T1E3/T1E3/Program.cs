﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1E3
{
    class Program
    {
        static void Main(string[] args)
        {
  
            Console.WriteLine("Hello, what is your name?");

            var name = (Console.ReadLine());

            Console.WriteLine($"Thanks, your name is {name}");

        }
    }
}
